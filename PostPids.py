import time
import hashlib
import grequests

def getData(pid):
    # You can use payloadstr or payloadobj. Depends on your preferences.
    # May be for text params is better use payloadobj
    #
    payloadstr = "submitted=1&pid={0}&clams=11&coins=11&event=11&personal=on&close=on&wid=5525e5d4".format(pid)

    payloadobj = {
        'submitted': 1,
        'pid': pid,
        'clams': 11,
        'coins': 11,
        'event': 11,
        'personal': 'on',
        'close': 'on',
        'wid': '5525e5d4',
    }

    return payloadobj if useDataObj else payloadstr

def postDataAsync(pid, session):
    return grequests.post(url, data=getData(pid), headers={'User-Agent': 'Me/1.0'}, session=session)

def getSession(maxRetries):
    import requests

    s = requests.Session()
    a = requests.adapters.HTTPAdapter(max_retries=maxRetries)
    b = requests.adapters.HTTPAdapter(max_retries=maxRetries)
    s.mount('http://', a)
    s.mount('https://', b)

    return s

if __name__ == '__main__':
    start = time.time()

    useDataObj = True  # type data to POST (string or post collection). Look at the  getData method.

    url = 'http://localhost:8080'

    pids = [hashlib.md5(str(i)).hexdigest() for i in range(1000)]  # some PIDs

    async_list = []  # A list to hold our things to do via async

    session = getSession(maxRetries=3)

    for pid in pids:
        async_list.append(postDataAsync(pid, session))

    responses = grequests.map(async_list, size=100)  # large size could throw an errors

    print 'Responses count', len(responses)

    print 'It took', time.time()-start, 'seconds.'
