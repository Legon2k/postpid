import SimpleHTTPServer
import SocketServer
import logging
import cgi


class ServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        logging.debug(self.headers)
        SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        logging.error(self.headers)

        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={
                'REQUEST_METHOD': 'POST',
                # 'CONTENT_TYPE': self.headers['Content-Type'],
            })

        for item in form.list:
            logging.info(item)

        SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)


if __name__ == '__main__':
    requestProcessed = 0

    logging.basicConfig(level=logging.DEBUG)

    Handler = ServerHandler

    PORT = 8080

    httpd = SocketServer.TCPServer(("", PORT), Handler)

    print "serving at port", PORT

    httpd.serve_forever()